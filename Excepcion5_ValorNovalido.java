import java.util.*;

public class Excepcion5_ValorNovalido{

 public static void main(String args[]){
 double x;
 
 try{
    x = leervalor();
    System.out.println("El numero cuadro es: "+ Math.pow(x,2.0));  
 }catch(ValorNovalido ex){
   System.out.println(ex.getMessage());
 }catch(InputMismatchException ex){
  System.out.println("Debe ingresar un numero");
 }
 
 }
 
 public static double leervalor() throws ValorNovalido{
  double n = 0.0;
  Scanner input = new Scanner(System.in);
  System.out.println("Ingrese un valor: ");
  n = input.nextDouble();
  
  if (n < 0){
    throw new ValorNovalido("El numero debe se positivo");
  }
  return n;
  
 }

}